terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"
}

provider "yandex" {
  service_account_key_file = file("/tmp/key.json")
  cloud_id                 = "b1g6d40oo1qv3bmu6des"
  folder_id                = "b1gprrvi7gm0rg9k426v"
  zone                     = "ru-central1-a"
}
