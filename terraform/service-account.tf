resource "yandex_iam_service_account" "editor-k8s" {
 name        = "editor-k8s"
 description = "editor-k8s"
}

resource "yandex_resourcemanager_folder_iam_member" "editor" {
 # Сервисному аккаунту назначается роль "editor".
 folder_id = "b1gprrvi7gm0rg9k426v"
 role      = "editor"
 member    = "serviceAccount:${yandex_iam_service_account.editor-k8s.id}"
}

resource "yandex_resourcemanager_folder_iam_member" "images-puller" {
 # Сервисному аккаунту назначается роль "container-registry.images.puller".
 folder_id = "b1gprrvi7gm0rg9k426v"
 role      = "container-registry.images.puller"
 member    = "serviceAccount:${yandex_iam_service_account.editor-k8s.id}"
}
