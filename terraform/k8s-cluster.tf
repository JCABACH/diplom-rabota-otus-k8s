resource "yandex_kubernetes_cluster" "otus-k8s-diplom" {
 name = "otus-k8s-diplom"
 release_channel = "RAPID"
 network_policy_provider = "CALICO"
 network_id = yandex_vpc_network.k8s-network.id
 master {
   version = "1.25"
   public_ip = "true"
   zonal {
     zone      = yandex_vpc_subnet.k8s-subnet.zone
     subnet_id = yandex_vpc_subnet.k8s-subnet.id
   }
 }
 service_account_id      = yandex_iam_service_account.editor-k8s.id
 node_service_account_id = yandex_iam_service_account.editor-k8s.id
   depends_on = [
     yandex_resourcemanager_folder_iam_member.editor,
     yandex_resourcemanager_folder_iam_member.images-puller
   ]
}